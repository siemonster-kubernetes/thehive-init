from __future__ import print_function
from commons import get_session, get_api_key
from cortex4py.api import Api as CortexApi
import os
try:
    from json.decoder import JSONDecodeError
except ImportError:
    JSONDecodeError = ValueError

ENDPOINT = os.environ.get('SERVICE_ENDPOINT', 'http://localhost:9000')
LOGIN = os.environ['ORG_ADMIN_LOGIN']
PASSWORD = os.environ['ORG_ADMIN_PASSWORD']

MISP_HOST = os.environ['MISP_HOST']
MISP_INTEGRATION_AUTH_KEY = os.environ['MISP_INTEGRATION_AUTH_KEY']

wait_for_misp = get_session(MISP_HOST, resource='/')
session = get_session(ENDPOINT)
api_key = get_api_key(ENDPOINT, LOGIN, PASSWORD)

api = CortexApi(ENDPOINT, api_key)

# enable legacy MISP 2.0 analyzer
try:
    analyzer_name = 'MISP_2_0'
    analyzer = api.analyzers.get_by_name(analyzer_name)
    if not analyzer:
        print('Enabling analyzer: %s...  ' % analyzer_name, end='')
        analyzer = api.analyzers.enable(analyzer_name, {
          "configuration": {
            "url": ["%s" % MISP_HOST] ,
            "key": ["%s" % MISP_INTEGRATION_AUTH_KEY],
          }
        })
        print('Done.')
    else:
      print('Analyzer %s is already enabled' % analyzer_name)
except:
    print('Analyzer %s not found' % analyzer_name)

# enable newest MISP 2.1 analyzer
try:
    analyzer_name = 'MISP_2_1'
    analyzer = api.analyzers.get_by_name(analyzer_name)
    if not analyzer:
        print('Enabling analyzer: %s...  ' % analyzer_name, end='')
        analyzer = api.analyzers.enable(analyzer_name, {
          "configuration": {
            "url": ["%s" % MISP_HOST] ,
            "key": ["%s" % MISP_INTEGRATION_AUTH_KEY],
          }
        })
        print('Done.')
    else:
      print('Analyzer %s is already enabled' % analyzer_name)
except:
    print('Analyzer %s not found' % analyzer_name)

print('Finished')
