# -*- coding: utf-8 -*-
import requests
import sys
import time


def exit_on_error(resp):
    if resp.status_code >= 400:
        print('')
        print('URL: %s Code: %s\n%s' % (resp.url, resp.status_code, resp.text))
        sys.exit(1)


def get_session(endpoint, resource='api/status', wait=True):
    session = requests.Session()
    while wait:
        try:
            response = session.get("%s/%s" % (endpoint, resource))
            if response.status_code == 200:
                break
            print('Waiting for %s became alive' % endpoint)
            time.sleep(1)
        except requests.exceptions.ConnectionError as e:
            print(e)
            time.sleep(5)

    response = session.get("%s/" % endpoint)
    exit_on_error(response)

    return session


def log_in(session, endpoint, login, password, retry=3):
    payload = {
        "user":"%s" % login,
        "password":"%s" % password,
    }
    try_num = 0
    while True:
        try_num += 1
        response = session.post("%s/%s" % (endpoint, 'api/login'), data=payload)
        if response.status_code <= 399 or try_num >= retry:
            break
        time.sleep(3)
    exit_on_error(response)
    return session


def get_api_key(endpoint, login, password):
    session = get_session(endpoint, wait=False)
    cookies = session.cookies.get_dict()
    if "CORTEX-XSRF-TOKEN" in cookies:
        session.headers = {**session.headers, **{"X-CORTEX-XSRF-TOKEN": cookies["CORTEX-XSRF-TOKEN"]}}
    if "THE-HIVE-XSRF-TOKEN" in cookies:
        session.headers = {**session.headers, **{"X-THE-HIVE-XSRF-TOKEN": cookies["THE-HIVE-XSRF-TOKEN"]}}

    session = log_in(session, endpoint, login, password)

    while True:
        response = session.get("%s/%s" % (endpoint, 'api/user/%s/key' % login))

        if response.status_code == 403 or response.status_code == 404:
            r = session.post("%s/%s" % (endpoint, 'api/user/%s/key/renew' % login))
            time.sleep(3)
            exit_on_error(r)

        if response.status_code == 200:
            return response.text

