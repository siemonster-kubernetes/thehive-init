FROM python:3.7-alpine

COPY ./ /app
WORKDIR /app
RUN apk -U add libmagic \
    && pip3 install -r requirements.txt \
    && chmod +x /app/entrypoint.sh

ENTRYPOINT ["/app/entrypoint.sh"]
