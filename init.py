from __future__ import print_function
from commons import exit_on_error, get_session, get_api_key
import os
import sys
try:
    from json.decoder import JSONDecodeError
except ImportError:
    JSONDecodeError = ValueError

ENDPOINT = os.environ.get('SERVICE_ENDPOINT', 'http://localhost:9000')
ADMIN_LOGIN = os.environ.get('SERVICE_ADMIN_LOGIN', 'admin')
ADMIN_NAME = os.environ.get('SERVICE_ADMIN_NAME', 'admin')
ADMIN_PASSWORD = os.environ.get('SERVICE_ADMIN_PASSWORD', 'admin')


def get_service_type(session):
    response = session.get("%s/%s" % (ENDPOINT, 'api/status'))
    exit_on_error(response)
    try:
        d = response.json()
    except JSONDecodeError as e:
        print(e)
        sys.exit(1)
    if "versions" not in d:
        print('wrong response format')
        sys.exit(1)

    if "TheHive" in d['versions']:
        return "TheHive", d['versions']['TheHive']

    if "Cortex" in d['versions']:
        return "Cortex", d['versions']['Cortex']
    print('unable to get service trype')
    sys.exit(1)


def get_new_user_payload(service, version=None):
    if service == "Cortex":
        return {
            "login": "%s" % ADMIN_LOGIN,
            "name": "%s" % ADMIN_NAME,
            "password": "%s" % ADMIN_PASSWORD,
            "organization": "cortex",
            "roles": [
                "superadmin"
            ],
        }
    if service == "TheHive":
        return {
            "login": "%s" % ADMIN_LOGIN,
            "name": "%s" % ADMIN_NAME,
            "password": "%s" % ADMIN_PASSWORD,
            "roles": [
                "read",
                "write",
                "admin",
                "alert"
            ]
        }


session = get_session(ENDPOINT)
service, version = get_service_type(session)

while True:
    response = session.get("%s/%s" % (ENDPOINT, 'api/user/current'))
    if 500 <= response.status_code <=599:
        print('Initialization...  ', end='')
        response = session.post("%s/%s" % (ENDPOINT, 'api/maintenance/migrate'))
        exit_on_error(response)
        print('Done')

    if response.status_code == 404:
        print('Creating a new user...  ',  end='')
        payload = get_new_user_payload(service, version)
        response = session.post("%s/%s" % (ENDPOINT, 'api/user'), data=payload)
        exit_on_error(response)
        # Implicitly generates an API key
        admin_api_key = get_api_key(ENDPOINT, ADMIN_LOGIN, ADMIN_PASSWORD)
        print('Done')

    if response.status_code == 401:
        print('Initialized')
        break
