from __future__ import print_function
from commons import get_session, get_api_key
from cortex4py.api import Api as CortexApi
import os
try:
    from json.decoder import JSONDecodeError
except ImportError:
    JSONDecodeError = ValueError
# import traceback

ENDPOINT = os.environ.get('SERVICE_ENDPOINT', 'http://localhost:9000')
LOGIN = os.environ['ORG_ADMIN_LOGIN']
PASSWORD = os.environ['ORG_ADMIN_PASSWORD']

SHUFFLE_HOST = os.environ['SHUFFLE_HOST']
SHUFFLE_KEY = os.environ['SHUFFLE_KEY']
SHUFFLE_WORKFLOW_ID = os.environ.get('SHUFFLE_WORKFLOW_ID', '7135366a-b3c8-4c90-98d9-c33780f0ae77')

wait_for_shuffle = get_session(SHUFFLE_HOST, resource='api/v1/_ah/health')
session = get_session(ENDPOINT)
api_key = get_api_key(ENDPOINT, LOGIN, PASSWORD)

api = CortexApi(ENDPOINT, api_key)

# enable & configure Shuffle responder
try:
    responder_name = 'Shuffle_1_0'
    print('Enabling responder: %s...  ' % responder_name, end='')

    api.do_post('organization/responder/{}'.format(responder_name), {
        "name": "%s" % responder_name,
        "configuration": {
            "url": "%s" % SHUFFLE_HOST,
            "api_key": "%s" % SHUFFLE_KEY,
            "workflow_id": "%s" % SHUFFLE_WORKFLOW_ID,
            "jobTimeout": 30,
            "check_tlp": True,
            "max_tlp": 2,
            "check_pap": True,
            "max_pap": 2
        },
        "jobTimeout": 30
    })

    print('Done.')
except Exception as e:
    pass
    # traceback.print_exc()
    # print('Failed to enable responder %s!' % responder_name)
    # print(e)

print('Finished')
