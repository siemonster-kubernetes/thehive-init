from __future__ import print_function
from commons import get_session, get_api_key
from cortex4py.api import Api as CortexApi
from cortex4py.models import Organization, User
import cortex4py.exceptions
import os
import requests
try:
    from json.decoder import JSONDecodeError
except ImportError:
    JSONDecodeError = ValueError

ENDPOINT = os.environ.get('SERVICE_ENDPOINT', 'http://localhost:9000')
ADMIN_LOGIN = os.environ.get('SERVICE_ADMIN_LOGIN', 'admin')
ADMIN_NAME = os.environ.get('SERVICE_ADMIN_NAME', 'admin')
ADMIN_PASSWORD = os.environ.get('SERVICE_ADMIN_PASSWORD', 'admin')

ORG_NAME = os.environ.get('ORG_NAME', 'demo')
ORG_DESCRIPTION = os.environ.get('ORG_DESCRIPTION', 'This is a demo organization')

USERS = [(
    os.environ.get('ORG_ADMIN_LOGIN', 'demo_admin'),
    os.environ.get('ORG_ADMIN_NAME', 'Demo Administrator'),
    os.environ.get('ORG_ADMIN_PASSWORD', 'demo'),
    ['read', 'analyze', 'OrgAdmin'],
),
(
    os.environ.get('ORG_INTEGRATION_USER', 'demo_integration'),
    os.environ.get('ORG_INTEGRATION_NAME', 'TheHive integration user'),
    os.environ.get('ORG_INTEGRATION_PASSWORD', 'integration'),
    ['read', 'analyze'],
)]

session = get_session(ENDPOINT)
admin_api_key = get_api_key(ENDPOINT, ADMIN_LOGIN, ADMIN_PASSWORD)
print("API_KEY:%s" % admin_api_key)
api = CortexApi(ENDPOINT, admin_api_key)

try:
    org = api.organizations.get_by_id(ORG_NAME)
except cortex4py.exceptions.NotFoundError:
    print('Creating a new organiztion...  ', end='')
    org = api.organizations.create(Organization({
        "name": "%s" % ORG_NAME,
        "description": "%s" % ORG_DESCRIPTION,
        "status": "Active",
    }))
    print('Done.')


for user, name, password, roles in USERS:
    try:
        new_user = api.users.get_by_id(user)
    except cortex4py.exceptions.NotFoundError:
        # Create a org admin user
        print('Creating a new user: %s...  ' % user, end='')
        new_user = api.users.create(User({
            'login': '%s' % user,
            'name': '%s' % name,
            'roles': roles,
            'status': 'Ok',
            'organization': ORG_NAME
        }))
        print('Done.')

    # Generate an API key
    if not new_user.hasKey:
        print("Generate a new API key for %s... " % user, end='')
        r = requests.post("%s/%s" % (ENDPOINT, 'api/user/%s/key/renew' % user),
                              headers={
                                  'Authorization': 'Bearer %s' % admin_api_key,
                                  'Content-Type': 'application/json'
                              }
                          )
        if r.status_code != 200:
            raise RuntimeError("Code: %s Message: %s" %(r.status_code, r.text))
        print('Done.')

    # Set password
    if not new_user.hasPassword:
        print("Set password for %s... " % user, end='')
        api.users.set_password(new_user.id, password)
        print('Done.')

print('Finished')
