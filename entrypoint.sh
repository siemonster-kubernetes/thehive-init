#!/usr/bin/env sh
set -ex

if [ $# -eq 0 ]; then
    exec /usr/bin/env sh
else
    for command in "$@"
    do
        python3 -u "$command"
    done
fi
