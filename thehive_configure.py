from __future__ import print_function
from commons import get_session, get_api_key

# [NOTE] at the moment (2020.10) thehive4py doesn't have methods to work with TheHive4,
# i.e. work with organisations, users, etc. So we have to work with API-calls directly
from thehive4py.api import TheHiveApi
from thehive4py.exceptions import TheHiveException

import os
import requests
import json
try:
    from json.decoder import JSONDecodeError
except ImportError:
    JSONDecodeError = ValueError

ENDPOINT = os.environ.get('SERVICE_ENDPOINT', 'http://localhost:9000')
ADMIN_LOGIN = os.environ.get('SERVICE_ADMIN_LOGIN', 'admin@thehive.local')
ADMIN_NAME = os.environ.get('SERVICE_ADMIN_NAME', 'admin')
ADMIN_PASSWORD = os.environ.get('SERVICE_ADMIN_PASSWORD', 'secret')

DEFAULT_ORG_NAME = os.environ.get('DEFAULT_ORG_NAME', 'SIEMonster')
DEFAULT_ORG_DESC = os.environ.get('DEFAULT_ORG_DESC', 'SIEMonster default organization')
ALERTING_PROFILE_NAME = 'SIEMonster Alerting Profile'

USERS = [
    (
        os.environ.get('ELASTALERT_LOGIN', 'elastalert@thehive.local'),
        os.environ.get('ELASTALERT_NAME', 'elastalert'),
        os.environ.get('ELASTALERT_PASSWORD', 'elastalert'),
        ALERTING_PROFILE_NAME,
    ),
    (
        os.environ.get('DEFAULT_USER_LOGIN', 'siemonster@thehive.local'),
        os.environ.get('DEFAULT_USER_NAME', 'siemonster'),
        os.environ.get('DEFAULT_USER_PASSWORD', 's13M0nSterZ'),
        'org-admin',
    )
]

session = get_session(ENDPOINT)
admin_api_key = get_api_key(ENDPOINT, ADMIN_LOGIN, ADMIN_PASSWORD)
print("API_KEY:%s" % admin_api_key)
api = TheHiveApi(ENDPOINT, admin_api_key)

# create default organisation if it's not exist
try:
    print('Create default organisation "%s" if it doesn\'t exist' % DEFAULT_ORG_NAME)
    req = api.url + '/api/organisation/%s' % DEFAULT_ORG_NAME
    org = requests.get(req, auth=api.auth).json()
    
    if 'id' in org:
        print('Organisation "%s" already exists' % DEFAULT_ORG_NAME)
    else:
        print('Organisation "%s" doesn\'t exist, adding it' % DEFAULT_ORG_NAME)
        
        # body with definitions for new organisation
        data = {
            "name": DEFAULT_ORG_NAME,
            "description": DEFAULT_ORG_DESC
        }
        
        # send request to create organisation
        req = api.url + '/api/organisation'
        new_org = requests.post(req, auth=api.auth, json=data).json()
        
        # check response
        if 'id' in new_org:
            print('Organisation "%s" has been successfully added' % DEFAULT_ORG_NAME)
        else:
            raise Exception("Failed to create organisation: {}".format(new_org))
except Exception as e:
    raise TheHiveException("Error on creating default organisation: {}".format(e))


# create default profile if it's not exist
try:
    print('Create alerting profile "%s" if it doesn\'t exist' % ALERTING_PROFILE_NAME)
    req = api.url + '/api/profile/%s' % ALERTING_PROFILE_NAME
    profile = requests.get(req, auth=api.auth).json()

    if 'id' in profile:
        print('Profile "%s" already exists' % ALERTING_PROFILE_NAME)
    else:
        print('Profile "%s" doesn\'t exist, adding it' % ALERTING_PROFILE_NAME)

        # body with definitions for new profile
        data = {
            "name": ALERTING_PROFILE_NAME,
            "permissions": [
                "manageAlert"
            ],
            "isAdmin": False
        }

        # # send request to create profile
        req = api.url + '/api/profile'
        new_profile = requests.post(req, auth=api.auth, json=data).json()

        # check response
        if 'id' in new_profile:
            print('Profile "%s" has been successfully added' % ALERTING_PROFILE_NAME)
        else:
            raise Exception("Failed to create profile: {}".format(new_profile))
except Exception as e:
    raise TheHiveException("Error on creating default profile: {}".format(e))

# create users
print("Create default users")
for login, name, password, profile in USERS:
    try:
        print('Create user "%s" if it doesn\'t exist' % login)
        req = api.url + '/api/v1/user/%s' % login
        user = requests.get(req, auth=api.auth).json()

        if 'id' in user:
            print('User "%s" already exists' % login)
        else:
            print('User "%s" doesn\'t exist, adding it' % login)

            # body with definitions for new user
            data = {
                "login": login,
                "name": name,
                "password": password,
                "profile": profile,
                "organisation": DEFAULT_ORG_NAME
            }

            # send request to create user
            req = api.url + '/api/v1/user'
            user = requests.post(req, auth=api.auth, json=data).json()
            # print(json.dumps(user, indent=2))

            # check response
            if '_id' in user:
                print('User "%s" has been successfully added' % login)
            else:
                raise Exception("Failed to create user: {}".format(user))

        # Generate an API key
        # print(json.dumps(user, indent=2))
        if 'hasKey' in user and not user['hasKey']:
            print("Generate a new API key for %s... " % login, end='')
            req = api.url + '/api/v1/user/%s/key/renew' % login
            r = requests.post(req, auth=api.auth)
            if r.status_code != 200:
                raise RuntimeError("Code: %s Message: %s" %(r.status_code, r.text))
            print('Done.')
    except Exception as e:
        raise TheHiveException("Error on creating default user: {}".format(e))

print('Finished')
